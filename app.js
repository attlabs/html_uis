
/**
 * Module dependencies.
 */
var express = require("express"),
	app     = express(),
	routes = require('./routes'),
	path = require('path'),
	http = require('http'),
	port    = parseInt(process.env.PORT, 10) || 3000;

app.configure(function(){
	app.use(express.favicon(__dirname + '/public/img/favicon.ico'));
	app.use(express.logger('dev'));
	app.use(express.methodOverride());
	app.use(express.bodyParser());
	app.use(function(req, res, next) {
		res.header("Access-Control-Allow-Origin", "*");
		res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
		res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
		next();
	});
	app.use(express.static(__dirname + '/app'));
	app.use(express.errorHandler({
		dumpExceptions: true,
		showStack: true
	}));
	app.use(app.router);
});

app.configure('development', function(){
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
	app.use(express.errorHandler());
});

app.all('/', function(req, res, next) {
	res.type('html');
	next();
});

app.get('/*', routes.index);

http.createServer(app).listen(port, function(){
  console.log('Express server listening on port ' + port);
});
