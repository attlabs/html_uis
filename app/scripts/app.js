'use strict';

angular.module('htmlUisApp', [])
	.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
		// For any unmatched url, send to /

		$routeProvider.when('/', {templateUrl: 'views/html5_gpu.html', controller: 'HtmlFiveGPUCtrl'})
			.when('/html5', {templateUrl: 'views/html5.html', controller: 'HtmlFiveCtrl'})
			.when('/html4', {templateUrl: 'views/html4.html', controller: 'HtmlFourCtrl'})
			.otherwise({redirectTo: '/'});

		$locationProvider.html5Mode(true);
	}]);
